package com.fab.android.demopayment

import android.annotation.SuppressLint
import java.util.*

object Utilities {

    @get:SuppressLint("DefaultLocale")
    val randomNumberString: String
        get() {
            val rnd = Random()
            val number = rnd.nextInt(999999)
            return String.format("%06d", number)
        }
}