package com.fab.android.demopayment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.fab.android.demopayment.databinding.ActivitySimulatorBinding
import com.fab.android.ikea.Ikea
import com.fab.android.ikea.utils.Logger


class SimulatorActivity : AppCompatActivity(), View.OnClickListener, Ikea.IkeaPaymentCallback {
    private val countries_list = arrayOf("IND", "ITA", "USA", "UKA", "ARE", "UAE")
    private val currency_list = arrayOf(
            "Indian Rupees",
            "Euro",
            "US Dollar",
            "British Pound",
            "United Arab Emirates Dirham",
            "United Arab Emirates Dirham"
    )
    private val currency_code_list = arrayOf("INR", "EUR", "USD", "GBR", "AED", "AED")
    private val pay_mode_list =
        arrayOf("Select Paymode", "Net Banking", "Credit Card", "Debit Card", "Wallet")
    private val expiry_month_list = arrayOf(
            "Select Month",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"
    )
    private val expiry_year_list = arrayOf(
            "Select Year",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"
    )
    private val user_logged_list = arrayOf("Select Option", "yes", "no")
    private val environment_list = arrayOf("Select environment", "Test", "Production")
    var environment_str = ""
    var country_code = ""
    var currency_type = ""
    var paymode = ""
    var expMonth = ""
    var expYear = ""
    var is_user_logged = ""
    private var binding: ActivitySimulatorBinding? = null
    private var merchantKey: String? = null
    private var merchantId: String? = null
    private var aggregator: String? = null
    private var builder: Ikea.Builder? = null
    private var environment = Ikea.Environment.TEST

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimulatorBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setFinishOnTouchOutside(false)
        builder = Ikea.Builder(this, this, this)
        aggregator = "paygate"
        if (environment == Ikea.Environment.PRODUCTION) {
//            merchantId = "201705240001";
            merchantId = "202110040001"
            //            merchantKey = "rXbmCVAcefiPyWZz9wVs9WBYPbaYVfDV4VxwowyJBHg=";
            merchantKey = "+6nCluPpB5BnOLLopZqfquMv104yAd8Wd9fEF6oY+GI="

            /*merchantId = "202105170004";
            merchantKey = "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8=";*/

            /*merchantId = "201905310001";
            merchantKey = "oB5Uj8okRjSWAwwEeA9aXqkEKMaislZa5Q0xrfg9kCU=";*/
        } else {
            /*merchantId = "201608020002";
            merchantKey = "U7BuLoPuNcR3hiZQnhzaP6qglwGIE9RF/fRF2EkF/hI=";*/

            //merchant hosted
            merchantId = "202105250008"
            merchantKey = "PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="

            //aggregator hosted
            /*merchantId = "202108240001";
            merchantKey = "FdmNoF51jfjt0Kv8338XMFsiZKu1tRlU8EMtPX6RCCU=";*/

            //aggregator hosted with paymode
            /*merchantId = "202108240004";
            merchantKey = "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q=";*/
        }
        val adapter_state = ArrayAdapter(this, R.layout.layout_list, countries_list)
        binding!!.countrySpinner.adapter = adapter_state
        val adapter_currency = ArrayAdapter(this, R.layout.layout_list, currency_list)
        binding!!.currencySpinner.adapter = adapter_currency
        val adapter_paymode = ArrayAdapter(this, R.layout.layout_list, pay_mode_list)
        binding!!.paymodeSpinner.adapter = adapter_paymode
        val adapter_expiry_month = ArrayAdapter(this, R.layout.layout_list, expiry_month_list)
        binding!!.expiryMonthSpinner.adapter = adapter_expiry_month
        val adapter_expiry_year = ArrayAdapter(this, R.layout.layout_list, expiry_year_list)
        binding!!.expiryYearSpinner.adapter = adapter_expiry_year
        val adapter_user_logged = ArrayAdapter(this, R.layout.layout_list, user_logged_list)
        binding!!.custLoggedSpinner.adapter = adapter_user_logged
        binding!!.custLoggedSpinner.setSelection(1)
        val adapter_environment = ArrayAdapter(this, R.layout.layout_list, environment_list)
        binding!!.environment.adapter = adapter_environment
        binding!!.countrySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        arg0: AdapterView<*>?,
                        arg1: View,
                        arg2: Int,
                        arg3: Long
                ) {
                    country_code = countries_list[arg2]
                    currency_type = currency_code_list[arg2]
                    binding!!.currencySpinner.setSelection(arg2)
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
        binding!!.currencySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        arg0: AdapterView<*>?,
                        arg1: View,
                        arg2: Int,
                        arg3: Long
                ) {
                    country_code = countries_list[arg2]
                    currency_type = currency_code_list[arg2]
                    binding!!.countrySpinner.setSelection(arg2)
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
        binding!!.paymodeSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        arg0: AdapterView<*>?,
                        arg1: View,
                        arg2: Int,
                        arg3: Long
                ) {
                    if (arg2 == 1) paymode = "NB" else if (arg2 == 2) paymode =
                        "CC" else if (arg2 == 3) paymode = "DC" else if (arg2 == 4) paymode = "WA"
                    //Log.d(TAG, "Safexpay - Paymode : " + paymode);
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
        binding!!.expiryYearSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        arg0: AdapterView<*>?,
                        arg1: View,
                        arg2: Int,
                        arg3: Long
                ) {
                    if (arg2 != 0) expYear = expiry_year_list[arg2]
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
        binding!!.expiryMonthSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        arg0: AdapterView<*>?,
                        arg1: View,
                        arg2: Int,
                        arg3: Long
                ) {
                    if (arg2 != 0) expMonth = expiry_month_list[arg2]
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
        binding!!.custLoggedSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        arg0: AdapterView<*>?,
                        arg1: View,
                        arg2: Int,
                        arg3: Long
                ) {
                    is_user_logged = if (arg2 == 2) "N" else "Y"
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
        binding!!.environment.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
                if (arg2 == 1) environment = Ikea.Environment.TEST else if (arg2 == 2) environment =
                    Ikea.Environment.PRODUCTION
                if (arg2 != 0) environment_str = environment_list[arg2]
            }

            override fun onNothingSelected(arg0: AdapterView<*>?) {}
        }
        binding!!.editOrderNumber.setText(com.fab.android.demopayment.Utilities.randomNumberString)
        binding!!.aggregatorId.setText(aggregator)
        binding!!.aggregatorId.isFocusable = false
        binding!!.aggregatorId.isClickable = false
        binding!!.editTransaction.setText("SALE")
        binding!!.editTransaction.isFocusable = false
        binding!!.editTransaction.isClickable = false
        binding!!.editChannel.setText("MOBILE")
        binding!!.editChannel.isFocusable = false
        binding!!.editChannel.isClickable = false
        binding!!.editAmount.setText("1")
        binding!!.editSuccessUrl.setText("http://localhost/ikea/response.php")
        binding!!.editFailureUrl.setText("http://localhost/ikea/response.php")
        binding!!.btPayment.setOnClickListener(this)
    }

    private fun transactionDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        val s5: String
        val s6: String
        val s7: String
        val s8: String
        s1 = binding!!.editOrderNumber.text.toString().trim { it <= ' ' }
        s2 = binding!!.editAmount.text.toString().trim { it <= ' ' }
        s3 = country_code
        s4 = currency_type
        s5 = binding!!.editTransaction.text.toString().trim { it <= ' ' }
        s6 = binding!!.editSuccessUrl.text.toString().trim { it <= ' ' }
        s7 = binding!!.editFailureUrl.text.toString().trim { it <= ' ' }
        s8 = binding!!.editChannel.text.toString().trim { it <= ' ' }

        //String txn_details =
        return "$aggregator|$merchantId|$s1|$s2|$s3|$s4|$s5|$s6|$s7|$s8"
    }

    private fun pgDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        s1 = binding!!.editPgId.text.toString().trim { it <= ' ' }
        s2 = paymode
        s3 = binding!!.editScheme.text.toString().trim { it <= ' ' }
        s4 = binding!!.editEmiMonths.text.toString().trim { it <= ' ' }
        val pgDetails = "$s1|$s2|$s3|$s4"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0 && s4.length == 0) {
            "|||"
        } else {
            pgDetails
        }
    }

    private fun cardDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        val s5: String
        s1 = binding!!.editCardNo.text.toString().trim { it <= ' ' }
        s2 = expMonth
        s3 = expYear
        s4 = binding!!.editCvv.text.toString().trim { it <= ' ' }
        s5 = binding!!.editCardName.text.toString().trim { it <= ' ' }
        val cardDetails = "$s1|$s2|$s3|$s4|$s5"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0 && s4.length == 0) {
            "||||"
        } else {
            cardDetails
        }
    }

    private fun customerDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        val s5: String
        s1 = binding!!.editCustName.text.toString().trim { it <= ' ' }
        s2 = binding!!.editCustEmail.text.toString().trim { it <= ' ' }
        s3 = binding!!.editCustMobile.text.toString().trim { it <= ' ' }
        s4 = binding!!.editCustUnique.text.toString().trim { it <= ' ' }
        s5 = is_user_logged
        val customer_details = "$s1|$s2|$s3|$s4|$s5"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0 && s5.length == 0) {
            "||||"
        } else {
            customer_details
        }
    }

    private fun billingDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        val s5: String
        s1 = binding!!.editBillAddress.text.toString().trim { it <= ' ' }
        s2 = binding!!.editBillCity.text.toString().trim { it <= ' ' }
        s3 = binding!!.editBillState.text.toString().trim { it <= ' ' }
        s4 = binding!!.editBillCountry.text.toString().trim { it <= ' ' }
        s5 = binding!!.editBillZip.text.toString().trim { it <= ' ' }
        val billing_details = "$s1|$s2|$s3|$s4|$s5"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0 && s4.length == 0 && s5.length == 0) {
            "||||"
        } else {
            billing_details
        }
    }

    private fun shippingDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        val s5: String
        val s6: String
        val s7: String
        s1 = binding!!.editShipAddress.text.toString().trim { it <= ' ' }
        s2 = binding!!.editShipCity.text.toString().trim { it <= ' ' }
        s3 = binding!!.editShipState.text.toString().trim { it <= ' ' }
        s4 = binding!!.editShipCountry.text.toString().trim { it <= ' ' }
        s5 = binding!!.editShipZip.text.toString().trim { it <= ' ' }
        s6 = binding!!.editShipDays.text.toString().trim { it <= ' ' }
        s7 = binding!!.editAddressCount.text.toString().trim { it <= ' ' }
        val shipping_details = "$s1|$s2|$s3|$s4|$s5|$s6|$s7"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0 && s4.length == 0 && s5.length == 0 && s6.length == 0 && s7.length == 0) {
            "||||||"
        } else {
            shipping_details
        }
    }

    private fun itemDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        s1 = binding!!.editItemCount.text.toString().trim { it <= ' ' }
        s2 = binding!!.editItemValue.text.toString().trim { it <= ' ' }
        s3 = binding!!.editItemCategory.text.toString().trim { it <= ' ' }
        val item_details = "$s1|$s2|$s3"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0) {
            "||"
        } else {
            item_details
        }
    }

    private fun otherDetails(): String {
        val s1: String
        val s2: String
        val s3: String
        val s4: String
        val s5: String
        s1 = binding!!.editUDF1.text.toString().trim { it <= ' ' }
        s2 = binding!!.editUDF2.text.toString().trim { it <= ' ' }
        s3 = binding!!.editUDF3.text.toString().trim { it <= ' ' }
        s4 = binding!!.editUDF4.text.toString().trim { it <= ' ' }
        s5 = binding!!.editUDF5.text.toString().trim { it <= ' ' }
        val other_details = "$s1|$s2|$s3|$s4|$s5"
        return if (s1.length == 0 && s2.length == 0 && s3.length == 0 && s4.length == 0 && s5.length == 0) {
            "||||"
        } else {
            other_details
        }
    }

    override fun onClick(v: View) {
        val id = v.id
        if (id == R.id.bt_payment) {
            if (!binding!!.merchantId.text.toString()
                    .isEmpty() && !binding!!.merchantKey.text.toString()
                    .isEmpty() && !environment_str.isEmpty()
            ) {
                merchantId = binding!!.merchantId.text.toString()
                merchantKey = binding!!.merchantKey.text.toString()
            }

            //builder.setMerchantDetail(aggregator, merchantId, merchantKey)
            builder!!.setMerchantDetail(
                    "fab",
                    "202110040001",
                    "+6nCluPpB5BnOLLopZqfquMv104yAd8Wd9fEF6oY+GI="
            )
                .setEnvironment(environment)
                .setOrderId(binding!!.editOrderNumber.text.toString())
                .setAmount(binding!!.editAmount.text.toString().toInt())
                .setTransactionMode(
                        country_code,
                        currency_type,
                        binding!!.editTransaction.text.toString(),
                        binding!!.editChannel.text.toString()
                )
                .setResponseUrl(
                        binding!!.editSuccessUrl.text.toString(),
                        binding!!.editFailureUrl.text.toString()
                )
                .setPgDetails(
                        binding!!.editPgId.text.toString().trim { it <= ' ' },
                        paymode,
                        binding!!.editScheme.text.toString().trim { it <= ' ' },
                        binding!!.editEmiMonths.text.toString().trim { it <= ' ' })
                .setCardDetails(
                        binding!!.editCardNo.text.toString().trim { it <= ' ' },
                        expMonth,
                        expYear,
                        binding!!.editCvv.text.toString().trim { it <= ' ' },
                        binding!!.editCardName.text.toString().trim { it <= ' ' })
                .setCustomerDetails(
                        binding!!.editCustName.text.toString().trim { it <= ' ' },
                        binding!!.editCustEmail.text.toString().trim { it <= ' ' },
                        binding!!.editCustMobile.text.toString().trim { it <= ' ' },
                        binding!!.editCustUnique.text.toString().trim { it <= ' ' },
                        is_user_logged.equals("Y", ignoreCase = true)
                )
                .setBillingDetails(
                        binding!!.editBillAddress.text.toString().trim { it <= ' ' },
                        binding!!.editBillCity.text.toString().trim { it <= ' ' },
                        binding!!.editBillState.text.toString().trim { it <= ' ' },
                        binding!!.editBillCountry.text.toString().trim { it <= ' ' },
                        binding!!.editBillZip.text.toString().trim { it <= ' ' })
                .setShippingDetails(
                        binding!!.editShipAddress.text.toString().trim { it <= ' ' },
                        binding!!.editShipCity.text.toString().trim { it <= ' ' },
                        binding!!.editShipState.text.toString().trim { it <= ' ' },
                        binding!!.editShipCountry.text.toString().trim { it <= ' ' },
                        binding!!.editShipZip.text.toString().trim { it <= ' ' },
                        binding!!.editShipDays.text.toString().trim { it <= ' ' },
                        binding!!.editAddressCount.text.toString().trim { it <= ' ' })
                .setItemDetails(
                        binding!!.editItemCount.text.toString().trim { it <= ' ' },
                        binding!!.editItemValue.text.toString().trim { it <= ' ' },
                        binding!!.editItemCategory.text.toString().trim { it <= ' ' })
                .setOtherDetails(
                        binding!!.editUDF1.text.toString().trim { it <= ' ' },
                        binding!!.editUDF2.text.toString().trim { it <= ' ' },
                        binding!!.editUDF3.text.toString().trim { it <= ' ' },
                        binding!!.editUDF4.text.toString().trim { it <= ' ' },
                        binding!!.editUDF5.text.toString().trim { it <= ' ' },
                        binding!!.editUDF6.text.toString().trim { it <= ' ' })
        }
        try {
            builder!!.startPayment()
        } catch (e: Exception) {
            Toast.makeText(this, "" + e.message, Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
    }

    override fun onRestart() {
        super.onRestart()
        binding!!.editOrderNumber.setText(com.fab.android.demopayment.Utilities.randomNumberString)
    }





    private fun onToastVisible(message: String) {
        runOnUiThread { Toast.makeText(baseContext, message, Toast.LENGTH_LONG).show() }
    }

    companion object {
        private val TAG: String = SimulatorActivity::class.java.getSimpleName()
    }

    override fun onInitiatePaymentComplete(
            txn_response: String?,
            pg_details: String?,
            fraud_details: String?,
            other_details: String?
    ) {
        onToastVisible("TRANSACTION SUCCESSFUL")
        Logger.d(TAG, """Safexpay - txn result - Success -
tnx_response 123232: $txn_response 
pg_detail : $pg_details 
fraud_detail : $fraud_details 
other_detail : $other_details""")
    }

    override fun onInitiatePaymentFailure(
            txn_response: String?,
            pg_details: String?,
            fraud_details: String?,
            other_details: String?
    ) {
        onToastVisible("TRANSACTION FAILED")
        Logger.d(TAG, """Safexpay - txn result - Failled -
tnx_response_26552542 : $txn_response 
pg_detail : $pg_details 
fraud_detail : $fraud_details 
other_detail : $other_details""")
    }

    override fun onInitiatePaymentCancelled(reasonMessage: String?) {
        onToastVisible("TRANSACTION CANCELLED")
        Logger.d(TAG, "Safexpay - Reason :$reasonMessage")
    }

    override fun onInitiatePaymentError(error: String?) {
        onToastVisible(error!!)
        Logger.d(TAG, "Safexpay - Reason :$error")
    }

    private fun clearFragmentStack() {
        for (fragment in supportFragmentManager.fragments) {
            supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }


}