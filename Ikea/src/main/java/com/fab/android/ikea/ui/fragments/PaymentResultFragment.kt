package com.fab.android.ikea.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.net.http.SslError
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.core.content.ContextCompat
import com.fab.android.ikea.PaymentActivity
import com.fab.android.ikea.R
import com.fab.android.ikea.constants.SessionStore
import com.fab.android.ikea.databinding.FragmentPaymentResultBinding
import com.fab.android.ikea.listener.PaymentResultCallback
import com.fab.android.ikea.ui.BaseFragment
import com.fab.android.ikea.utils.CryptoUtils
import com.fab.android.ikea.utils.Logger
import com.fab.android.ikea.utils.Logger.d
import pl.aprilapps.easyphotopicker.*
import java.net.URLDecoder
import java.util.*

class PaymentResultFragment (htmlContent: String) : BaseFragment() {
    private var binding: FragmentPaymentResultBinding? = null
    private var htmlContent = ""
    private var activity: PaymentActivity? = null
    private var dialog: Dialog? = null
    private var paymentResultCallback: PaymentResultCallback? = null
    private var filePathCallbackArray: ValueCallback<Array<Uri>>? = null
    private var easyImage: EasyImage? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentPaymentResultBinding.inflate(inflater, container, false)
        activity = getActivity() as PaymentActivity?
        init()
        return binding!!.root
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun init() {
        WebView.setWebContentsDebuggingEnabled(true)
        binding!!.resultUrlViewSdk.settings.javaScriptEnabled = true
        binding!!.resultUrlViewSdk.settings.allowFileAccess = true
        binding!!.resultUrlViewSdk.settings.domStorageEnabled = true
        easyImage = EasyImage.Builder(requireContext())
            .setChooserType(ChooserType.CAMERA_AND_GALLERY)
            .setChooserTitle("Tabby Image Chooser")
            .build()
        binding!!.resultUrlViewSdk.visibility = View.VISIBLE
        dialog = Dialog(requireContext())
        dialog!!.setTitle("Transaction Processing")
        dialog!!.setContentView(R.layout.custom_progress_bar)
        //dialog.show();
        Handler().postDelayed({
            binding!!.resultUrlViewSdk.loadData(
                htmlContent,
                "text/html",
                "utf-8"
            )
            binding!!.resultUrlViewSdk.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    super.onPageFinished(view, url)
                    if (activity!!.dialog!!.isShowing) activity!!.dialog!!.dismiss()
                    SessionStore.isResultPage = true
                }

                override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
//                    super.onReceivedSslError(view, handler, error)
                    if (error?.primaryError == SslError.SSL_UNTRUSTED){
                        Logger.e(TAG, "Safexpay -- SSL_UNTRUSTED -- $error")
                    }else if (error?.primaryError == SslError.SSL_EXPIRED){
                        Logger.e(TAG, "Safexpay -- SSL_EXPIRED -- $error")
                    }else if (error?.primaryError == SslError.SSL_IDMISMATCH){
                        Logger.e(TAG, "Safexpay -- SSL_IDMISMATCH -- $error")
                    }else if (error?.primaryError == SslError.SSL_NOTYETVALID){
                        Logger.e(TAG, "Safexpay -- SSL_NOTYETVALID -- $error")
                    } else if (error?.primaryError == SslError.SSL_INVALID){
                        Logger.e(TAG, "Safexpay -- SSL_NOTYETVALID -- $error")
                    }
                    handler?.proceed()
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    view.loadUrl(url)
                    try {
                        d(TAG, "Safexpay - Called URL : $url")

                        /*String successUrl = "", failureUrl = "";
                        if ((SessionStore.SUCCESS_URL).contains("?")) {
                            Logger.d(TAG, "Safexpay - url :" + (SessionStore.SUCCESS_URL).substring(0, (SessionStore.SUCCESS_URL).lastIndexOf("?")));
                            successUrl = (SessionStore.SUCCESS_URL).substring(0, (SessionStore.SUCCESS_URL).lastIndexOf("?"));
                        } else successUrl = SessionStore.SUCCESS_URL;
                        if ((SessionStore.FAILURE_URL).contains("?")) {
                            Logger.d(TAG, "Safexpay - url :" + (SessionStore.FAILURE_URL).substring(0, (SessionStore.FAILURE_URL).lastIndexOf("?")));
                            failureUrl = (SessionStore.FAILURE_URL).substring(0, (SessionStore.FAILURE_URL).lastIndexOf("?"));
                        } else failureUrl = SessionStore.FAILURE_URL;

                        Logger.d(TAG, "Safexpay - URLs : \n" + successUrl + "\n" + failureUrl + "\n");*/

                        //if (url.contains(successUrl) || url.contains(failureUrl)) {
                        if (url.contains("txn_response") && url.contains("pg_details") && url.contains(
                                "fraud_details"
                            ) && url.contains("other_details")
                        ) {
                            val txn_response_enc = CryptoUtils.decrypt(
                                Uri.parse(url).getQueryParameter("txn_response"),
                                SessionStore.merchantKey
                            )
                            val pg_details = CryptoUtils.decrypt(
                                Uri.parse(url).getQueryParameter("pg_details"),
                                SessionStore.merchantKey
                            )
                            val fraud_details = CryptoUtils.decrypt(
                                Uri.parse(url).getQueryParameter("fraud_details"),
                                SessionStore.merchantKey
                            )
                            val other_details = CryptoUtils.decrypt(
                                Uri.parse(url).getQueryParameter("other_details"),
                                SessionStore.merchantKey
                            )
                            var txn_response = ""
                            txn_response = if (txn_response_enc.contains("%")) URLDecoder.decode(
                                txn_response_enc
                            ) else txn_response_enc
                            val arrayTxnResponse =
                                txn_response.split("\\|").dropLastWhile { it.isEmpty() }
                                    .toTypedArray()
                            val arrayPgDetails =
                                pg_details.split("\\|").dropLastWhile { it.isEmpty() }
                                    .toTypedArray()
                            val arrayFraudDetails =
                                fraud_details.split("\\|").dropLastWhile { it.isEmpty() }
                                    .toTypedArray()
                            val arrayOtherDetails =
                                other_details.split("\\|").dropLastWhile { it.isEmpty() }
                                    .toTypedArray()
                            d(
                                TAG,
                                "Safexpay - Result - encrypted txn data : " + Uri.parse(url)
                                    .getQueryParameter("txn_response")
                            )
                            d(
                                TAG,
                                "Safexpay - Result - encrypted pg data : " + Uri.parse(url)
                                    .getQueryParameter("pg_details")
                            )
                            d(
                                TAG,
                                "Safexpay - Result - encrypted fraud data : " + Uri.parse(url)
                                    .getQueryParameter("fraud_details")
                            )
                            d(
                                TAG,
                                "Safexpay - Result - encrypted other data : " + Uri.parse(url)
                                    .getQueryParameter("other_details")
                            )
                            d(TAG, "Safexpay - Result - decrypted txn data : $txn_response_enc")
                            d(TAG, "Safexpay - Result - decrypted txn data decode : $txn_response")
                            d(TAG, "Safexpay - Result - decrypted pg data : $pg_details")
                            d(TAG, "Safexpay - Result - decrypted fraud data : $fraud_details")
                            d(TAG, "Safexpay - Result - decrypted other data : $other_details")
                            d(
                                TAG,
                                "Safexpay - Result - array txn data : " + Arrays.toString(
                                    arrayTxnResponse
                                )
                            )
                            d(
                                TAG,
                                "Safexpay - Result - array pg data : " + Arrays.toString(
                                    arrayPgDetails
                                )
                            )
                            d(
                                TAG,
                                "Safexpay - Result - array fraud data : " + Arrays.toString(
                                    arrayFraudDetails
                                )
                            )
                            d(
                                TAG,
                                "Safexpay - Result - array other data : " + Arrays.toString(
                                    arrayOtherDetails
                                )
                            )
                            if (arrayTxnResponse.size != 0) {
                                val isSuccessful = checkStatus(arrayTxnResponse)
                                invalidateUI(
                                    isSuccessful,
                                    arrayTxnResponse,
                                    txn_response,
                                    pg_details,
                                    fraud_details,
                                    other_details
                                )
                            }
                        }
                    } catch (e: Exception) {
                        view.loadUrl(url)
                        d(TAG, "Safexpay - Error : " + e.message)
                    }
                    /*try {
                        if (!url.endsWith("checkPay")) {
                            String encryptedResp = CryptoUtils.decrypt(Uri.parse(url).getQueryParameter("txn_response"),
                                    SessionStore.merchantKey);
                            String[] valueData = encryptedResp.split("\\|");
                            if (valueData.length != 0) {
                                boolean isSuccessful = checkStatus(valueData);
                                invalidateUI(isSuccessful, valueData);
                            }
                        }
                    } catch (Exception e) {
                        binding.resultUrlViewSdk.loadUrl(url);
                        e.printStackTrace();
                    }*/return  false
                }
            }
            binding!!.resultUrlViewSdk.webChromeClient = object : WebChromeClient() {
                override fun onShowFileChooser(
                    webView: WebView,
                    filePathCallback: ValueCallback<Array<Uri>>,
                    fileChooserParams: FileChooserParams
                ): Boolean {
                    if (filePathCallbackArray != null) {
                        filePathCallbackArray!!.onReceiveValue(null)
                    }
                    filePathCallbackArray = filePathCallback
                    openImageChooser()
                    return true
                }
            }
        }, 3000)

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun invalidateUI(isSuccess: Boolean, valueData: Array<String>, txn_response: String, pg_details: String, fraud_details: String, other_details: String) {
        binding!!.resultUrlViewSdk.visibility = View.GONE
        if (isSuccess) {
            SessionStore.successOrderId = valueData[2]
            SessionStore.successPaymentId = valueData[8]
            SessionStore.successTransactionId = valueData[9]
            PaymentActivity.paymentResult = getString(R.string.transaction_successful)
            /* binding.paymentResultHeaderTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultAmountTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultStatusIvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultStatusTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultOrderNoTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultStatusIvSdk.setImageDrawable(getResources().getDrawable(R.drawable.ic_success));
            binding.paymentResultStatusTvSdk.setTextColor(getResources().getColor(R.color.blueColorDark));
            binding.paymentResultStatusTvSdk.setText(getText(R.string.transaction_successful));
            binding.paymentResultAmountTvSdk.setText(String.format("%s%s", SessionStore.currencySymbol, SessionStore.orderAmount));
            binding.paymentResultOrderNoTvSdk.setText(String.format(getString(R.string.order_no_s), SessionStore.orderId));*/
        } else {
            PaymentActivity.paymentResult = getString(R.string.transaction_failed)
            /*binding.paymentResultHeaderTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultAmountTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultStatusIvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultStatusTvSdk.setVisibility(View.VISIBLE);
            binding.paymentResultStatusTvSdk.setText(getText(R.string.transaction_failed));
            binding.paymentResultStatusIvSdk.setImageDrawable(getResources().getDrawable(R.drawable.ic_cross_error));
            binding.paymentResultStatusTvSdk.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            binding.paymentResultAmountTvSdk.setText(String.format("%s%s", SessionStore.CURRENCY, SessionStore.orderAmount));
            binding.paymentResultOrderNoTvSdk.setText(String.format(getString(R.string.order_no_s), SessionStore.orderId));*/
        }
        paymentResultCallback = activity?.setPaymentListener()
        paymentResultCallback!!.paymentData(txn_response, pg_details, fraud_details, other_details)
    }

    private fun checkStatus(elementsOfUrl: Array<String>): Boolean {
        for (element in elementsOfUrl) {
            if (element.equals("successful", ignoreCase = true)) return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage!!.handleActivityResult(requestCode, resultCode, data, requireActivity(), object : DefaultCallback() {
            override fun onMediaFilesPicked(mediaFiles: Array<MediaFile>, mediaSource: MediaSource) {
                if (filePathCallbackArray != null) {
                    val callback: ValueCallback<Array<Uri>> = filePathCallbackArray as ValueCallback<Array<Uri>>
                    val results: MutableList<Uri> = ArrayList()
                    //Uri[] results = new Uri[]{};
                    for (file in mediaFiles) {
                        results.add(Uri.fromFile(file.file.absoluteFile))
                    }
                    callback.onReceiveValue(results.toTypedArray())
                    filePathCallbackArray = null
                }
            }

            override fun onCanceled(source: MediaSource) {
                super.onCanceled(source)
                if (filePathCallbackArray != null) {
                    val callback: ValueCallback<Array<Uri>> = filePathCallbackArray as ValueCallback<Array<Uri>>
                    callback.onReceiveValue(arrayOf())
                    filePathCallbackArray = null
                }
            }

            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                super.onImagePickerError(error, source)
                if (filePathCallbackArray != null) {
                    val callback: ValueCallback<Array<Uri>> = filePathCallbackArray as ValueCallback<Array<Uri>>
                    callback.onReceiveValue(arrayOf())
                    filePathCallbackArray = null
                }
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CHOOSER_PERMISSIONS_REQUEST_CODE && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            easyImage!!.openChooser(this)
        } else {
            easyImage!!.openDocuments(this)
        }
    }

    private fun openImageChooser() {
        val perms = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (arePermissionsGranted(perms)) {
            easyImage!!.openChooser(this@PaymentResultFragment)
        } else {
            requestPermissions(perms, CHOOSER_PERMISSIONS_REQUEST_CODE)
        }
    }

    private fun arePermissionsGranted(permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(requireContext(), permission) != 0) {
                return false
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        binding!!.resultUrlViewSdk.onPause()
    }

    override fun onResume() {
        super.onResume()
        binding!!.resultUrlViewSdk.onResume()
    }

    companion object {
        private val TAG = PaymentResultFragment::class.java.simpleName
        private const val CHOOSER_PERMISSIONS_REQUEST_CODE = 1050
    }

    init {
        this.htmlContent = htmlContent
    }


}