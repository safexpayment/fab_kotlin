package com.fab.android.ikea.services

import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface IkeaService {

    @POST("agcore/appPay")
    fun makePayment(@Body jsonObject: JsonObject?): Call<ResponseBody?>?
}