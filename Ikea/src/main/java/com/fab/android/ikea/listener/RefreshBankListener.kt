package com.fab.android.ikea.listener

interface RefreshBankListener {
    fun onRefresh()
}