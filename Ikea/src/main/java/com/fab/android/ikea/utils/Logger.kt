package com.fab.android.ikea.utils

import android.util.Log
import com.fab.android.ikea.BuildConfig

object Logger {

    /**
     * Logs debug messages if DEBUG mode
     */
    fun d(tag: String?, message: String?) {
        if (BuildConfig.DEBUG) Log.d(tag, message!!)
    }

    /**
     * Logs all the errors
     */
    fun e(tag: String?, message: String?) {
        Log.e(tag, message!!)
    }
}