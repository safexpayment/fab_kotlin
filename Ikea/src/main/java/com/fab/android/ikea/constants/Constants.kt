package com.fab.android.ikea.constants

object Constants {

 /**
  * Extra Bundle key passed to SAFEXPAY SDK.
  */
 const val PAYMENT_BUNDLE = "payment_bundle"

 /**
  * Extra Bundle key for Order No which is passed back from SDK.
  */
 const val ORDER_NO = "orderNo"

 /**
  * Extra key for the Order object that is sent through Bundle.
  */
 const val ORDER = "order"

 /**
  * Activity request code
  */
 const val REQUEST_CODE = 9

 /**
  * Internal key for all transactions
  */
 const val internalKey = "HiktfH0Mhdla4zDg0/4ASwFQh2OS+nf9MVL0ik3DsmE="

 /**
  * key for the Juspay card URL
  */
 const val URL = "url"

 /**
  * Key for the merchant ID for the current transaction
  */
 const val MERCHANT_ID = "merchantId"

 /**
  * Key for the success Url for the current transaction
  */
 const val SUCCESS_URL = "successUrl"

 /**
  * Key for the failure Url for the current transaction
  */
 const val FAILURE_URL = "failureUrl"

 /**
  * Key for the countryCode for the current transaction
  */
 const val COUNTRY_CODE = "countryCode"

 /**
  * Key for the AMOUNT for the current transaction
  */
 const val AMOUNT = "amount"
 const val AMOUNT_SUB_CHARGE = "sub_charge"
 const val AMOUNT_PRO_CHARGE = "pro_charge"

 /**
  * Key for the CURRENCY for the current transaction
  */
 const val CURRENCY = "currency"

 /**
  * Key for the transaction type for the current transaction
  */
 const val TXN_TYPE = "txn_type"

 /**
  * Key for the CHANNEL for the current transaction
  */
 const val CHANNEL = "channel"

 /**
  * Key for the merchant KEY for the current transaction
  */
 const val MERCHANT_KEY = "merchantKey"

 /**
  * Key for the aggregator ID for the current transaction
  */
 const val AGGREGATOR_ID = "aggregator_id"

 /**
  * Key for Netbanking Data passed to Juspay
  */
 const val POST_DATA = "postData"

 /**
  * Key for transactionID in the return bundle
  */
 const val TRANSACTION_ID = "transactionID"

 /**
  * Key for paymentID in the return bundle
  */
 const val PAYMENT_ID = "paymentID"
 const val PAYMENT_STATUS_SUCCESS = "SUCCESS"

 /**
  * Status code for UPI Pending Authentication
  */
 const val PENDING_PAYMENT = 2
 const val KEY_CODE = "code"
 const val KEY_MESSAGE = "message"

 /**
  * key for the PG details
  */
 const val PG_DETAILS = "pg_details"

 /**
  * key for card details
  */
 const val CARD_DETAILS = "card_details"

 /**
  * key for customer details
  */
 const val CUST_DETAILS = "cust_details"

 /**
  * key for the UPI details
  */
 const val UPI_DETAILS = "upi_details"

 /**
  * key for Billing details
  */
 const val BILLING_DETAILS = "billing_details"

 /**
  * key for Shipping details
  */
 const val SHIPPING_DETAILS = "shipping_details"

 /**
  * key for Item details
  */
 const val ITEM_DETAILS = "item_details"

 /**
  * key for Other details
  */
 const val OTHER_DETAILS = "other_details"

 /**
  * result code
  */
 const val ACTION_INTENT_FILTER = "com.safexpay.android.sdk"
 const val RESULT_SUCCESS = 1
 const val RESULT_CANCELLED = 2
 const val RESULT_FAILED = 3
 const val RESULT_ERROR = 4

 /**
  * callback intent name
  */
 const val PAYMENT_LISTENER = "payment_listener"
}