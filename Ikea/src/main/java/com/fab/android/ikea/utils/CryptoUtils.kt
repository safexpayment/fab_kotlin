package com.fab.android.ikea.utils

import android.org.apache.commons.codec.binary.Base64
import android.org.apache.commons.codec.binary.Base64.*
import android.util.Log
import okio.internal.commonAsUtf8ToByteArray
import java.io.UnsupportedEncodingException
import java.security.Key

import java.security.spec.AlgorithmParameterSpec

import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

import okio.ByteString.Companion.decodeBase64
import java.security.MessageDigest


class CryptoUtils {

    companion object {
        private const val ENCRYPTION_IV = "0123456789abcdef"
        private const val PADDING = "AES/CBC/PKCS5Padding"
        private const val ALGORITHM = "AES"
        private const val CHARTSET = "UTF-8"

        /**
         * Encryption
         *
         * @param textToEncrypt for which text we encrypt
         * @param key           for which key to encryption
         * @return encrypted value
         */
   /*     fun encrypt(textToEncrypt: String, key: String): String {
            return try {
                val cipher = Cipher.getInstance(PADDING)
                cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv())
                Log.d("safex6532553252",String(decodeBase64(cipher.doFinal(textToEncrypt.toByteArray()))))
               String(decodeBase64(cipher.doFinal(textToEncrypt.toByteArray())))
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }*/

        fun encrypt(textToEncrypt: String, key: String): String {
            return try {
                val cipher = Cipher.getInstance(PADDING)
                cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv())
                String(android.org.apache.commons.codec.binary.Base64.encodeBase64(cipher.doFinal(textToEncrypt.toByteArray())))
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }



        /**
         * Decryption
         *
         * @param textToDecrypt for which text we decrypt
         * @param key           for which key to decryption
         * @return decrypted value
         */
        fun decrypt(textToDecrypt: String?, key: String): String {
            return try {
                val cipher = Cipher.getInstance(PADDING)
                cipher.init(Cipher.DECRYPT_MODE, makeKey(key), makeIv())
                String(cipher.doFinal(Base64.decodeBase64(textToDecrypt)))
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }

        private fun makeIv(): AlgorithmParameterSpec? {
            try {
                return IvParameterSpec(ENCRYPTION_IV.toByteArray(charset(CHARTSET)))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
            return null
        }

        private fun makeKey(encryptionKey: String): Key? {
            try {
                val key = Base64.decodeBase64(encryptionKey)
                return SecretKeySpec(key, ALGORITHM)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        fun generateMerchantKey(): String? {
            var newKey: String? = null
            try {
                // Get the KeyGenerator
                val kgen = KeyGenerator.getInstance("AES")
                kgen.init(256) // 128, 192 and 256 bits available

                // Generate the secret key specs.
                val skey = kgen.generateKey()
                val raw = skey.encoded
                newKey = String(encodeBase64(raw))
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return newKey
        }

        fun sha256Checksum(base: String): String {
            return try {
                val digest = MessageDigest.getInstance("SHA-256")
                val hash = digest.digest(base.toByteArray(charset("UTF-8")))
                val hexString = StringBuffer()
                for (byteHash in hash) {
                    val hex = Integer.toHexString(0xff and byteHash.toInt())
                    if (hex.length == 1) hexString.append('0')
                    hexString.append(hex)
                }
                hexString.toString()
            } catch (ex: Exception) {
                throw RuntimeException(ex)
            }
        }
    }

    init {
        try {
            val field = Class.forName("javax.crypto.JceSecurity").getDeclaredField(
                "isRestricted")
            field.isAccessible = true
            field[null] = Boolean
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}