package com.fab.android.ikea.listener

interface PaymentResultCallback {

    fun paymentData(txn_response: String?, pg_details: String?, fraud_details: String?, other_details: String?)
}