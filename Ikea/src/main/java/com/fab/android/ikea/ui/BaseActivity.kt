package com.fab.android.ikea.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity(){
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        val calligraphyClass = calligraphyClass
        if (calligraphyClass != null) {
            try {
                val method = calligraphyClass.getMethod("wrap", Context::class.java)
                super.attachBaseContext(method.invoke(calligraphyClass, newBase) as Context)
                return
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private val calligraphyClass: Class<*>?
        private get() = try {
            Class.forName("uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper")
        } catch (e: ClassNotFoundException) {
            null
        }

    /**
     * Update the actionbar accordingly
     */
    protected fun updateActionBar() {
        if (supportActionBar == null) {
            return
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
    }

    /**
     * returnResult is used to pass back the appropriate result to the initiator activity that
     * started this activity for result. The method will call [Activity.finish] and the
     * current activity will be stopped.
     *
     * @param bundle if any extra params that need to be passed back.
     * @param result Appropriate Activity result to be sent back to caller Activity - [Activity.RESULT_OK]
     * on Success or [Activity.RESULT_CANCELED] on Failure.
     */
    fun returnResult(bundle: Bundle?, result: Int) {
        val intent = Intent()
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        setResult(result, intent)
        finish()
    }

    /**
     * Overloading method for [BaseActivity.returnResult] with null Bundle.
     *
     * @param result Appropriate Activity result to be sent back to caller Activity - [Activity.RESULT_OK]
     * on Success or [Activity.RESULT_CANCELED] on Failure.
     */
    fun returnResult(result: Int) {
        returnResult(null, result)
    }

    /**
     * Hides the Soft keyboard if visible.
     */
    fun hideKeyboard() {
        val view = currentFocus
        if (view != null) {
            (getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

    /**
     * Update the actionbar of the associated activity
     *
     * @param title - title to be set to current activity's actionbar
     */
    fun updateActionBarTitle(@StringRes title: Int) {
        if (supportActionBar == null) {
            return
        }
        supportActionBar!!.setTitle(title)
    }
}