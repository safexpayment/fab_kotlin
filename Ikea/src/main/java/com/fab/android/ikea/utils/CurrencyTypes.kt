package com.fab.android.ikea.utils

enum class CurrencyTypes(var countryCode: String, var currencyType: String, var currencySymbol: String) {

    ARE("ARE", "AED", "AED"), UAE("UAE", "AED", "AED"), INR("IND", "INR", "₹"), USD("USA", "USD", "$"), GBP("UK", "GBP", "£");
    fun matches(currencyType: String): Boolean {
        return this.currencyType == currencyType
    }
}