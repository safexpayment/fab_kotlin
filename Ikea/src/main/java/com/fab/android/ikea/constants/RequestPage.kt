package com.fab.android.ikea.constants

object RequestPage {
    fun getPaymentPage(txn_details: String?, pg_details: String?, card_details: String?, customer_details: String?, billing_details: String?, shipping_details: String?, item_details: String?, other_details: String?, me_id: String?, baseUrl: String?): String {
        val page = "<!--Process transaction-->" +
                "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                "<html>" +
                "<head>" +
                "<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>" +
                "<title>Payment Gateway</title>" +
                "</head>" +
                "<body OnLoad='OnLoadEvent();'>" +
                "<form name='RedirectForm' action='%s/agcore/payment' method='post'>" +
                "<input type='hidden' id='pg_details' name='pg_details' value='%s' />" +
                "<input type='hidden' id='other_details' name='other_details' value='%s' />" +
                "<input type='hidden' id='me_id' name='me_id' value='%s' />" +
                "<input type='hidden' id='txn_details' name='txn_details' value='%s' />" +
                "<input type='hidden' id='card_details' name='card_details' value='%s' />" +
                "<input type='hidden' id='cust_details' name='cust_details' value='%s' />" +
                "<input type='hidden' id='bill_details' name='bill_details' value='%s' />" +
                "<input type='hidden' id='ship_details' name='ship_details' value='%s' />" +
                "<input type='hidden' id='item_details' name='item_details' value='%s' />" +  //"<input type='hidden' id='upi_details' name='upi_details' value='%s' />" +
                "</form>" +
                "<script language='JavaScript'>" +
                "function OnLoadEvent() {document.RedirectForm.submit();}" +
                "</script>" +
                "</body>" +
                "</html>"
        return String.format(page, baseUrl, pg_details, other_details, me_id,
            txn_details, card_details, customer_details, billing_details,
            shipping_details, item_details)
    }
}