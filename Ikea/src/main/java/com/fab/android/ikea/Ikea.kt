package com.fab.android.ikea

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.fab.android.ikea.constants.Constants
import com.fab.android.ikea.services.ServiceGenerator
import com.fab.android.ikea.utils.CryptoUtils
import com.fab.android.ikea.utils.Logger
import java.util.*

class Ikea : BroadcastReceiver {
    private var activity: Activity? = null
    var context: Context? = null
        private set
    private var environment: Environment? = null
    private var aggregator: String? = null
    private var merchantId: String? = null
    private var merchantKey: String? = null
    private var orderId: String? = null
    private var orderAmount = 0
    private var currency: String? = null
    private var txnType: String? = null
    private var channel: String? = null
    private var countryCode: String? = null
    private var successUrl: String? = null
    private var failureUrl: String? = null
    private var mCallback: IkeaPaymentCallback? = null
    private var pg_details: String? = null
    private var customer_details: String? = null
    private var card_details: String? = null
    private var billing_details: String? = null
    private var shipping_details: String? = null
    private var item_details: String? = null
    private var other_details: String? = null



    private constructor(builder: Builder){
        activity = builder.activity
        context = builder.context
        mCallback = builder.callback
        environment = if (builder.environment == null) Environment.TEST else builder.environment
        aggregator = builder.aggregator
        merchantId = builder.merchantId
        merchantKey = builder.merchantKey
        orderId = builder.orderId
        orderAmount = builder.orderAmount
        currency = if (builder.currency == "") "AED" else builder.currency
        txnType = if (builder.txnType == "") "SALE" else builder.txnType
        channel = if (builder.channel == "") "MOBILE" else builder.channel
        countryCode = if (builder.countryCode == "") "ARE" else builder.countryCode
        successUrl = if (builder.successUrl == "") "http://localhost/ikea/response.php" else builder.successUrl
        failureUrl = if (builder.failureUrl == "") "http://localhost/ikea/response.php" else builder.failureUrl
        pg_details = if (builder.pg_details == "") "|||" else builder.pg_details
        customer_details = if (builder.customer_details == "") "||||Y" else builder.customer_details
        card_details = if (builder.card_details == "") "||||" else builder.card_details
        billing_details = if (builder.billing_details == "") "||||" else builder.billing_details
        shipping_details = if (builder.shipping_details == "") "||||||" else builder.shipping_details
        item_details = if (builder.item_details == "") "||" else builder.item_details
        other_details = if (builder.other_details == "") "|||||" else builder.other_details
        environment?.let { ServiceGenerator.initialize(it) }
        initiatePayment()


    }

    private constructor() {
        //this is require
    }


    override fun onReceive(context: Context?, intent: Intent?) {



        val keyCode = intent!!.getIntExtra(Constants.KEY_CODE, 0)
        val keyMessage = intent.getStringExtra(Constants.KEY_MESSAGE)

        if (keyCode == Constants.RESULT_SUCCESS) {
            if (intent.extras != null) {
                mCallback!!.onInitiatePaymentComplete(
                        intent.extras!!.getString("txn_response"),
                        intent.extras!!.getString("pg_details"),
                        intent.extras!!.getString("fraud_details"),
                        intent.extras!!.getString("other_details")
                )
                Logger.d("TAG", "Safexpay - txn result builder - " +
                        "\ntnx_response : " + intent.getExtras()?.getString("txn_response") +
                        " \npg_detail : " + intent.getExtras()?.getString("pg_details") +
                        " \nfraud_detail : " + intent.getExtras()?.getString("fraud_details") +
                        " \nother_detail : " + intent.getExtras()?.getString("other_details"));
            }
        }else if (keyCode == Constants.RESULT_FAILED){

            if (intent.extras != null) {
                mCallback!!.onInitiatePaymentFailure(
                        intent.extras!!.getString("txn_response"),
                        intent.extras!!.getString("pg_details"),
                        intent.extras!!.getString("fraud_details"),
                        intent.extras!!.getString("other_details")
                )
                /*Logger.d(TAG, "Safexpay - txn result builder - " +
                        "\ntnx_response : " + intent.getExtras().getString("txn_response") +
                        " \npg_detail : " + intent.getExtras().getString("pg_details") +
                        " \nfraud_detail : " + intent.getExtras().getString("fraud_details") +
                        " \nother_detail : " + intent.getExtras().getString("other_details"));*/
            }
        } else if (keyCode == Constants.RESULT_CANCELLED) mCallback!!.onInitiatePaymentCancelled(
                keyMessage
        ) else if (keyCode == Constants.RESULT_ERROR) mCallback!!.onInitiatePaymentError(keyMessage)

    }
    enum class Environment {
        TEST, PRODUCTION
    }
    interface IkeaPaymentCallback {
        fun onInitiatePaymentComplete(
                txn_response: String?,
                pg_details: String?,
                fraud_details: String?,
                other_details: String?
        )
        fun onInitiatePaymentFailure(
                txn_response: String?,
                pg_details: String?,
                fraud_details: String?,
                other_details: String?
        )
        fun onInitiatePaymentCancelled(reasonMessage: String?)
        fun onInitiatePaymentError(error: String?)
    }

    private fun initiatePayment(){
        try {
            if (activity != null && orderId != null && !orderId!!.trim { it <= ' ' }.isEmpty() && orderAmount >= 1){

                val intent = Intent(context, PaymentActivity::class.java)
                val bundle = Bundle()
                bundle.putString(Constants.AGGREGATOR_ID, aggregator)
                bundle.putString(Constants.MERCHANT_KEY, merchantKey)
                bundle.putString(Constants.ORDER_NO, orderId)
                bundle.putString(Constants.AMOUNT, orderAmount.toString())
                bundle.putString(Constants.CURRENCY, currency)
                bundle.putString(Constants.TXN_TYPE, txnType)
                bundle.putString(Constants.CHANNEL, channel)
                bundle.putString(Constants.SUCCESS_URL, successUrl)
                bundle.putString(Constants.FAILURE_URL, failureUrl)
                bundle.putString(Constants.COUNTRY_CODE, countryCode)
                /*bundle.putString(Constants.MERCHANT_ID,
                    merchantId?.let { CryptoUtils.encrypt(it, Constants.internalKey) })*/
                bundle.putString(Constants.MERCHANT_ID, CryptoUtils.encrypt(merchantId!!, Constants.internalKey))
                bundle.putString(Constants.PG_DETAILS, pg_details)
                bundle.putString(Constants.CUST_DETAILS, customer_details)
                bundle.putString(Constants.CARD_DETAILS, card_details)
                bundle.putString(Constants.BILLING_DETAILS, billing_details)
                bundle.putString(Constants.SHIPPING_DETAILS, shipping_details)
                bundle.putString(Constants.ITEM_DETAILS, item_details)
                bundle.putString(Constants.OTHER_DETAILS, other_details)
                intent.putExtras(bundle)
                activity!!.startActivity(intent)

            }else{
                mCallback!!.onInitiatePaymentError(
                        Objects.requireNonNull(activity)!!.getString(R.string.data_not_accurate)
                )

            }

        }catch (e: Exception){
            mCallback!!.onInitiatePaymentError(
                    Objects.requireNonNull(activity)!!.getString(R.string.data_not_accurate)
            )

        }
    }

    class Builder(
            public val activity: Activity,
            public val context: Context,
            public val callback: IkeaPaymentCallback
    ){
        public var environment: Environment? = Environment.TEST
        public var aggregator: String? = ""
        public var merchantId: String? = ""
        public var merchantKey: String? = ""
        public var orderId: String? = ""
        public var orderAmount = 0
        public var currency = "AED"
        public var txnType = "SALE"
        public var channel = "MOBILE"
        public var countryCode = "ARE"
        public var successUrl = "http://localhost/ikea/response.php"
        public var failureUrl = "http://localhost/ikea/response.php"
        public var pg_details = "|||"
        public var customer_details = "||||Y"
        public var card_details = "||||"
        public var billing_details = "||||"
        public var shipping_details = "||||||"
        public var item_details = "||"
        public var other_details = "|||||"
        fun setEnvironment(environment: Environment): Builder {
            this.environment = environment
            return this
        }

        fun setMerchantDetail(aggregator: String, merchantId: String, merchantKey: String?): Builder {
            this.aggregator = aggregator
            this.merchantId = merchantId
            this.merchantKey = merchantKey
            return this
        }

        fun setOrderId(orderId: String): Builder {
            this.orderId = orderId
            return this
        }

        fun setAmount(orderAmount: Int): Builder {
            this.orderAmount = orderAmount
            return this
        }

        fun setTransactionMode(
                countryCode: String,
                currency: String,
                txnType: String,
                channel: String
        ): Builder {
            this.currency = currency
            this.txnType = txnType
            this.channel = channel
            this.countryCode = countryCode
            return this
        }

        fun setResponseUrl(successUrl: String, failureUrl: String): Builder {
            this.successUrl = successUrl
            this.failureUrl = failureUrl
            return this
        }

        fun setPgDetails(pg_id: String, paymode: String, scheme_id: String, emi_months: String): Builder {

            /*if (!pg_id.isEmpty() && !paymode.isEmpty() && !scheme_id.isEmpty())*/
            pg_details = "$pg_id|$paymode|$scheme_id|$emi_months"
            return this
        }

        fun setCustomerDetails(
                customer_full_name: String,
                customer_mail_id: String,
                customer_mobile_number: String,
                customer_unique_id: String,
                customer_is_login: Boolean
        ): Builder {
            /*if (!customer_full_name.isEmpty() && !customer_mail_id.isEmpty() && !customer_mobile_number.isEmpty())*/
            if (customer_is_login) customer_details = "$customer_full_name|$customer_mail_id|$customer_mobile_number|$customer_unique_id|Y" else customer_details = "$customer_full_name|$customer_mail_id|$customer_mobile_number|$customer_unique_id|N"
            return this
        }

        fun setCardDetails(
                card_number: String,
                card_ex_month: String,
                card_ex_year: String,
                card_cvv: String,
                card_holder_name: String
        ): Builder {
            /*if (!card_number.isEmpty() && !card_ex_month.isEmpty() && !card_ex_year.isEmpty() && !card_cvv.isEmpty())*/
            card_details = "$card_number|$card_ex_month|$card_ex_year|$card_cvv|$card_holder_name"
            return this
        }

        fun setBillingDetails(
                billing_address: String,
                billing_city: String,
                billing_state: String,
                billing_country: String,
                billing_zip: String
        ): Builder {
            billing_details = "$billing_address|$billing_city|$billing_state|$billing_country|$billing_zip"
            return this
        }

        fun setShippingDetails(
                shipping_address: String,
                shipping_city: String,
                shipping_state: String,
                shipping_country: String,
                shipping_zip: String,
                shipping_day: String,
                shipping_address_count: String
        ): Builder {
            shipping_details = "$shipping_address|$shipping_city|$shipping_state|$shipping_country|$shipping_zip|$shipping_address_count|$shipping_day"
            return this
        }

        fun setItemDetails(item_count: String, item_value: String, item_category: String): Builder {
            if (!item_count.isEmpty() && !item_value.isEmpty() && !item_category.isEmpty()) item_details = "$item_count|$item_value|$item_category"
            return this
        }

        fun setOtherDetails(
                udf1: String,
                udf2: String,
                udf3: String,
                udf4: String,
                udf5: String,
                udf6: String
        ): Builder {
            other_details = "$udf1|$udf2|$udf3|$udf4|$udf5|$udf6"
            return this
        }

        fun startPayment(): Ikea? {
            require(
                    !(aggregator == null || merchantId == null || merchantKey == null || orderId == null || orderId!!.isEmpty() || orderAmount < 1 ||
                            aggregator!!.isEmpty() || merchantId!!.isEmpty() || merchantKey!!.isEmpty())
            ) { "Valid data is required to pay with IKEA." }
            /*else {
                if (!pg_details.isEmpty() && customer_details.isEmpty()) {
                    throw new IllegalArgumentException("When you pass pgDetails then customerDetails is mandatory.");
                }
            }*/

            /*String[] custArray = customer_details.split("\\|");
            if (!(custArray.length >= 4) || !(Validators.isValidName(custArray[0])) ||
                    !(Validators.isValidEmail(custArray[1])) || !(Validators.isValidMobile(custArray[2]))) {
                throw new IllegalArgumentException("Valid customer data is required to pay with SafeXPay.");
            }*/mInstance = Ikea(this)
            return mInstance
        }

        companion object {
            private var mInstance: Ikea? = null
            @JvmStatic
            val instance: Ikea?
                get() {
                    if (mInstance == null) synchronized(Ikea::class.java) { if (mInstance == null) mInstance = Ikea() }
                    return mInstance
                }
        }

    }


}