package com.fab.android.ikea

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.fab.android.ikea.Ikea.Builder.Companion.instance
import com.fab.android.ikea.constants.Constants
import com.fab.android.ikea.constants.RequestPage
import com.fab.android.ikea.constants.SessionStore
import com.fab.android.ikea.databinding.ActivityPaymentBinding
import com.fab.android.ikea.listener.CancelTransactionListener
import com.fab.android.ikea.listener.PaymentResultCallback
import com.fab.android.ikea.ui.BaseActivity
import com.fab.android.ikea.ui.BaseFragment
import com.fab.android.ikea.ui.fragments.PaymentResultFragment
import com.fab.android.ikea.utils.CryptoUtils
import com.fab.android.ikea.utils.Logger
import com.fab.android.ikea.utils.Utilities
import com.google.gson.JsonObject
import java.util.*

class PaymentActivity : BaseActivity(), View.OnClickListener, CancelTransactionListener, PaymentResultCallback {
    var dialog: Dialog? = null
    var countDownTimer: CountDownTimer? = null
    private var binding: ActivityPaymentBinding? = null
    private var resourceId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setFinishOnTouchOutside(false)
        init()
    }

    override fun onClick(v: View?) {

    }

    override fun cancelClicked(reason: String?) {

    }

    override fun paymentData(txn_response: String?, pg_details: String?, fraud_details: String?, other_details: String?) {
        txn_response?.let { pg_details?.let { it1 -> fraud_details?.let { it2 -> other_details?.let { it3 -> paymentResultProcessed(it, it1, it2, it3) } } } }
    }

    override fun onDestroy() {
        unregisterReceiver(instance)
        SessionStore.clearSession()
        paymentResult = ""
        super.onDestroy()
    } //

    private fun init() {

        dialog = Dialog(this)
        dialog!!.setTitle("Transaction Processing")
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.custom_progress_bar)
        dialog!!.show()
        AGGREGATOR_ID = Objects.requireNonNull(intent.extras)!!.getString(Constants.AGGREGATOR_ID)
        ORDER_NO = Objects.requireNonNull(intent.extras)!!.getString(Constants.ORDER_NO)
        ORDER_AMOUNT = Objects.requireNonNull(intent.extras)!!.getString(Constants.AMOUNT)
        COUNTRY_CODE = Objects.requireNonNull(intent.extras)!!.getString(Constants.COUNTRY_CODE)
        CURRENCY = Objects.requireNonNull(intent.extras)!!.getString(Constants.CURRENCY)
        TXN_TYPE = Objects.requireNonNull(intent.extras)!!.getString(Constants.TXN_TYPE)
        SUCCESS_URL = Objects.requireNonNull(intent.extras)!!.getString(Constants.SUCCESS_URL)
        FAILURE_URL = Objects.requireNonNull(intent.extras)!!.getString(Constants.FAILURE_URL)
        CHANNEL = Objects.requireNonNull(intent.extras)!!.getString(Constants.CHANNEL)
        SessionStore.merchantId = intent.getStringExtra(Constants.MERCHANT_ID)!!
        SessionStore.merchantKey = intent.getStringExtra(Constants.MERCHANT_KEY)!!
        SessionStore.AGGREGATOR_ID = AGGREGATOR_ID!!
        SessionStore.ORDER_NO = ORDER_NO!!
        SessionStore.ORDER_AMOUNT = ORDER_AMOUNT!!
        SessionStore.COUNTRY_CODE = COUNTRY_CODE!!
        SessionStore.CURRENCY = CURRENCY!!
        SessionStore.TXN_TYPE = TXN_TYPE!!
        SessionStore.SUCCESS_URL = SUCCESS_URL!!
        SessionStore.FAILURE_URL = FAILURE_URL!!
        SessionStore.CHANNEL = CHANNEL!!
        SessionStore.PG_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.PG_DETAILS)!!
        SessionStore.CUSTOMER_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.CUST_DETAILS)!!
        SessionStore.CARD_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.CARD_DETAILS)!!
        // SessionStore.UPI_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.UPI_DETAILS)!!
        SessionStore.BILLING_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.BILLING_DETAILS)!!
        SessionStore.SHIPPING_DETAILS = Objects.requireNonNull(intent.extras)?.getString(Constants.SHIPPING_DETAILS)!!
        SessionStore.ITEM_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.ITEM_DETAILS)!!
        SessionStore.OTHER_DETAILS = Objects.requireNonNull(intent.extras)!!.getString(Constants.OTHER_DETAILS)!!
        resourceId = Utilities.getCurrencyType(intent.getStringExtra(Constants.CURRENCY)).currencySymbol
        SessionStore.currencySymbol = resourceId.toString()
        observerViewModel()
        if (SessionStore.merchantId == null || SessionStore.merchantKey == null || SessionStore.merchantId.isEmpty() || SessionStore.merchantKey.isEmpty()) {
            fireBroadcastAndReturn(Constants.RESULT_ERROR, null, "MID or M-Key not present in request!")
            finish()
        }
        val filter = IntentFilter(Constants.ACTION_INTENT_FILTER)
        registerReceiver(instance, filter)
    }

    private fun observerViewModel() {
        bindDataWithView()

    }

    private fun counter() {
        countDownTimer = object : CountDownTimer(60 * 1000, 1000) {
            @SuppressLint("DefaultLocale")
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                fireBroadcastAndReturn(Constants.RESULT_ERROR, null, "Something is wrong!")
                //loadPaymentResultFragment("Time out");
            }
        }
    }

    private fun fireBroadcastAndReturn(resultCode: Int, data: Bundle?, message: String?) {
        val intent = Intent()
        if (data != null) intent.putExtras(data)
        if (message != null && !message.isEmpty()) intent.putExtra(Constants.KEY_MESSAGE, message)
        intent.putExtra(Constants.KEY_CODE, resultCode)
        intent.action = Constants.ACTION_INTENT_FILTER
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)

        sendBroadcast(intent)
        SessionStore.clearSession()

        /*val intent = Intent()
        if (data != null) {
            intent.putExtras(data)
        }
        intent.putExtra(Constants.KEY_CODE, resultCode)
        if (message != null && !message.isEmpty()) {
            intent.putExtra(Constants.KEY_MESSAGE, message)
        }
        intent.action = Constants.ACTION_INTENT_FILTER
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        intent.putExtra("FinishMsg","ACTIVITY_FINISH: Network broken.");
        sendBroadcast(intent);*/

        // Finish current activity
        finish()
    }

    companion object {
        private val TAG = PaymentActivity::class.java.simpleName
        var paymentResult = ""
        private var AGGREGATOR_ID: String? = ""
        private var ORDER_NO: String? = ""
        private var COUNTRY_CODE: String? = ""
        private var ORDER_AMOUNT: String? = ""
        private var CURRENCY: String? = ""
        private var TXN_TYPE: String? = ""
        private var SUCCESS_URL: String? = ""
        private var FAILURE_URL: String? = ""
        private var CHANNEL: String? = ""
    }

    private fun bindDataWithView() {

        preparePayment(SessionStore.PG_DETAILS, SessionStore.CARD_DETAILS, SessionStore.CUSTOMER_DETAILS, SessionStore.BILLING_DETAILS,
                SessionStore.SHIPPING_DETAILS, SessionStore.ITEM_DETAILS, SessionStore.OTHER_DETAILS, SessionStore.UPI_DETAILS)
    }

    fun preparePayment(pg_details: String, card_details: String, customer_details: String, billing_details: String, shipping_details: String, item_details: String, other_details: String, upi_details: String?) {

        val txn_pg_details: String
        val txn_card_details: String
        val txn_customer_details: String
        val txn_billing_details: String
        val txn_shipping_details: String
        val txn_item_details: String
        val txn_other_details: String
        var txn_upi_details: String

        val jsonObject = JsonObject()
        val meId = CryptoUtils.decrypt(SessionStore.merchantId, Constants.internalKey)
        val txn_details = (AGGREGATOR_ID + "|" + meId + "|" + ORDER_NO + "|" + ORDER_AMOUNT + "|" + COUNTRY_CODE
                + "|" + CURRENCY + "|" + TXN_TYPE + "|" + SUCCESS_URL + "|" + FAILURE_URL + "|" + CHANNEL)
        txn_pg_details = if (!pg_details.replace("|", "").trim { it <= ' ' }.isEmpty()) pg_details else "|||"
        txn_card_details = if (!card_details.replace("|", "").trim { it <= ' ' }.isEmpty()) card_details else "||||"
        txn_customer_details = if (!customer_details.replace("|", "").trim { it <= ' ' }.isEmpty()) customer_details else "||||Y"
        txn_billing_details = if (!billing_details.replace("|", "").trim { it <= ' ' }.isEmpty()) billing_details else "||||"
        txn_shipping_details = if (!shipping_details.replace("|", "").trim { it <= ' ' }.isEmpty()) shipping_details else "||||||"
        txn_item_details = if (!item_details.replace("|", "").trim { it <= ' ' }.isEmpty()) item_details else "||"
        txn_other_details = if (!other_details.replace("|", "").trim { it <= ' ' }.isEmpty()) other_details else "|||||"

        jsonObject.addProperty("txn_details", CryptoUtils.encrypt(txn_details, SessionStore.merchantKey))
        jsonObject.addProperty("pg_details", CryptoUtils.encrypt(txn_pg_details, SessionStore.merchantKey))
        jsonObject.addProperty("card_details", CryptoUtils.encrypt(txn_card_details, SessionStore.merchantKey))
        jsonObject.addProperty("cust_details", CryptoUtils.encrypt(txn_customer_details, SessionStore.merchantKey))
        jsonObject.addProperty("bill_details", CryptoUtils.encrypt(txn_billing_details, SessionStore.merchantKey))
        jsonObject.addProperty("ship_details", CryptoUtils.encrypt(txn_shipping_details, SessionStore.merchantKey))
        jsonObject.addProperty("item_details", CryptoUtils.encrypt(txn_item_details, SessionStore.merchantKey))
        jsonObject.addProperty("other_details", CryptoUtils.encrypt(txn_other_details, SessionStore.merchantKey))
        //jsonObject.addProperty("upi_details", CryptoUtils.encrypt(txn_upi_details, SessionStore.merchantKey));
        jsonObject.addProperty("me_id", meId)
        //detailsViewModel.makePayment(jsonObject);

        //Debug logger
        Logger.d(TAG, """
     Safexpay - 
     Decrypted txn_details : $txn_details
     Encrypted txn_details : ${CryptoUtils.encrypt(txn_details, SessionStore.merchantKey)}
     """.trimIndent())
        Logger.d(TAG, """
     Safexpay - 
     Decrypted pg_details : $txn_pg_details
     Encrypted pg_details : ${CryptoUtils.encrypt(txn_pg_details, SessionStore.merchantKey)}
     """.trimIndent())
        Logger.d(TAG, """
     Safexpay - 
     Decrypted cust_details : $txn_customer_details
     Encrypted cust_details : ${CryptoUtils.encrypt(txn_customer_details, SessionStore.merchantKey)}
     """.trimIndent())
        Logger.d(TAG, """
     Safexpay - 
     Decrypted card_details : $txn_card_details
     Encrypted card_details : ${CryptoUtils.encrypt(txn_card_details, SessionStore.merchantKey)}
     """.trimIndent())
        Logger.d(TAG, """
     Safexpay - 
     Decrypted bill_details : $txn_billing_details
     Encrypted bill_details : ${CryptoUtils.encrypt(txn_billing_details, SessionStore.merchantKey)}
     """.trimIndent())
        Logger.d(TAG, """
     Safexpay - 
     Decrypted ship_details : $txn_shipping_details
     Encrypted ship_details : ${CryptoUtils.encrypt(txn_shipping_details, SessionStore.merchantKey)}
     """.trimIndent())
        Logger.d(TAG, """
     Safexpay - 
     Decrypted item_details : $txn_item_details
     Encrypted item_details : ${CryptoUtils.encrypt(txn_item_details, SessionStore.merchantKey)}
     """.trimIndent())
        //Logger.d(TAG, "Safexpay - \nDecrypted upi_details : " + txn_upi_details + "\nEncrypted upi_details : " + CryptoUtils.encrypt(txn_upi_details, SessionStore.merchantKey));
        Logger.d(TAG, """
     Safexpay - 
     Decrypted other_details : $txn_other_details
     Encrypted other_details : ${CryptoUtils.encrypt(txn_other_details, SessionStore.merchantKey)}
     """.trimIndent())

        //txn object debug logger
        Logger.d(TAG, "Safexpay - \nEncrypted txn : $jsonObject")
        val page = RequestPage.getPaymentPage(
                CryptoUtils.encrypt(txn_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_pg_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_card_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_customer_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_billing_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_shipping_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_item_details, SessionStore.merchantKey),
                CryptoUtils.encrypt(txn_other_details, SessionStore.merchantKey),
                meId,
                SessionStore.baseUrl
        )
        Logger.d(TAG, "Safexpay - \nPAGE : " + page);

        /*String hashRequest = meId + "~" + ORDER_NO + "~" + ORDER_AMOUNT + "~" + COUNTRY_CODE + "~" + CURRENCY;
        String hash = CryptoUtils.sha256Checksum(hashRequest);
        String merchant_request = txn_details + "~" + txn_pg_details + "~" + txn_card_details + "~"
                + txn_customer_details + "~" + txn_billing_details + "~" + txn_shipping_details
                + "~" + txn_item_details + "~" + txn_upi_details + "~" + txn_other_details;
        String page = RequestPageChecksum.getPaymentPage(
                CryptoUtils.encrypt(merchant_request, SessionStore.merchantKey),
                CryptoUtils.encrypt(hash, SessionStore.merchantKey),
                meId, SessionStore.baseUrl
        );

        Logger.d(TAG, "Safexpay - \nDecrypted merchant_request : " + merchant_request + "\nEncrypted merchant_request : " + CryptoUtils.encrypt(merchant_request, SessionStore.merchantKey));
        Logger.d(TAG, "Safexpay - \nDecrypted hash : " + hashRequest + "\nChecksum : " + hash + "\n Encrypted hash : " + CryptoUtils.encrypt(hash, SessionStore.merchantKey));
        Logger.d(TAG, "Safexpay - \nME ID : " + meId);*/
        loadPaymentResultFragment(page)


    }

    private fun loadPaymentResultFragment(htmlContent: String) {
        clearFragmentStack()
        loadFragment(PaymentResultFragment(htmlContent), false)
        //binding.apBtnPay.setVisibility(View.GONE);
    }

    private fun clearFragmentStack() {
        for (fragment in supportFragmentManager.fragments) {
            supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    private fun fetchMerchantBrandingDetails() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("me_id", SessionStore.merchantId)
        // detailsViewModel!!.getBrandingData(jsonObject)
    }

    fun loadFragment(fragment: BaseFragment, addToBackStack: Boolean) {
        //binding.apBtnPay.setVisibility(View.VISIBLE);
        /*binding.screenModeTV.setText(fragment.getFragmentName());
        if (!fragment.getFragmentName().equals(getString(R.string.payment_methods)))
            binding.apTvBack.setVisibility(View.VISIBLE);*/
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.replace(R.id.ap_rel_container, fragment)
        if (addToBackStack) fragmentTransaction.addToBackStack(fragment.fragmentName)
        fragmentTransaction.commit()
    }

    fun setPaymentListener(): PaymentResultCallback {
        return this
    }

    override fun onBackPressed() {
        if (paymentResult == "") {
            if (supportFragmentManager.backStackEntryCount == 0) else supportFragmentManager.popBackStackImmediate()
        } else {
            finish()
        }
    }

    private fun paymentResultProcessed(txn_response: String, pg_details: String, fraud_details: String, other_details: String) {
        try {
            Logger.d(TAG, """Safexpay - txn result - 
tnx_response : $txn_response 
pg_detail : $pg_details 
fraud_detail : $fraud_details 
other_detail : $other_details""")
            val bundleData = Bundle()
            bundleData.putString("txn_response", txn_response)
            bundleData.putString("pg_details", pg_details)
            bundleData.putString("fraud_details", fraud_details)
            bundleData.putString("other_details", other_details)
            //bundleData.putStringArray("",new String[]{});


            val txn = txn_response.split("\\|".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val txnStatus = txn[10].split("~".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            Logger.d(TAG, """Safexpay - txn result - txnStatus : $txnStatus""")
            if (txnStatus[0].equals("Successful", ignoreCase = true) && txn[11] == "0") fireBroadcastAndReturn(Constants.RESULT_SUCCESS, bundleData, null) else fireBroadcastAndReturn(Constants.RESULT_FAILED, bundleData, null)


            /*Intent intent = new Intent();
            if ((txnStatus[0]).equalsIgnoreCase("Successful") && txn[11].equals("0"))
                intent.putExtra(Constants.KEY_CODE, Constants.RESULT_SUCCESS);
            else intent.putExtra(Constants.KEY_CODE, Constants.RESULT_FAILED);

            intent.putExtra("txn_response", txn_response);
            intent.putExtra("pg_details", pg_details);
            intent.putExtra("fraud_details", fraud_details);
            intent.putExtra("other_details", other_details);
            intent.setAction(Constants.ACTION_INTENT_FILTER);
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            sendBroadcast(intent);*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}