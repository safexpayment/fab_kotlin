package com.fab.android.ikea.services

import com.fab.android.ikea.Ikea
import com.fab.android.ikea.constants.SessionStore
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.lang.RuntimeException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object ServiceGenerator {

    private val TAG = ServiceGenerator::class.java.simpleName
    private const val PRODUCTION_BASE_URL = "https://bankfab.safexpay.com"
    private const val TEST_BASE_URL = "https://safexpayuat.bankfab.com"

    fun initialize(environment: Ikea.Environment) {
        val baseUrl = if (environment == Ikea.Environment.PRODUCTION) PRODUCTION_BASE_URL else TEST_BASE_URL
        SessionStore.baseUrl = baseUrl
    }

    init {
        initialize(Ikea.Environment.TEST)
    }














    /*  private val TAG = ServiceGenerator::class.java.simpleName
      private const val PRODUCTION_BASE_URL = "https://bankfab.safexpay.com"
      private const val TEST_BASE_URL = "https://safexpayuat.bankfab.com"
      private val builder = Retrofit.Builder()
          .client(unsafeOkHttpClient.build())
          .addConverterFactory(GsonConverterFactory.create())
      private var retrofit: Retrofit? = null
      fun initialize(environment: Ikea.Environment) {
          val baseUrl =
              if (environment == Ikea.Environment.PRODUCTION) PRODUCTION_BASE_URL else TEST_BASE_URL
          SessionStore.baseUrl = baseUrl
          retrofit = builder.baseUrl(baseUrl).build()
      }

      val safeXService: IkeaService
          get() = retrofit!!.create(IkeaService::class.java)

      // Create a trust manager that does not validate certificate chains
      val unsafeOkHttpClient: Retrofit.Builder

      // Install the all-trusting trust manager

          // Create an ssl socket factory with our all-trusting manager
          get() = try {
              // Create a trust manager that does not validate certificate chains
              val trustAllCerts = arrayOf<TrustManager>(
                      object : X509TrustManager {
                          @Throws(CertificateException::class)
                          override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                          }

                          @Throws(CertificateException::class)
                          override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                          }

                          override fun getAcceptedIssuers(): Array<X509Certificate> {
                              return arrayOf()
                          }
                      }
              )

              // Install the all-trusting trust manager
              val sslContext = SSLContext.getInstance("SSL")
              sslContext.init(null, trustAllCerts, SecureRandom())

              // Create an ssl socket factory with our all-trusting manager
              val sslSocketFactory = sslContext.socketFactory
              val builder = Retrofit.Builder()
              builder.readTimeout(60, TimeUnit.SECONDS)
              builder.connectTimeout(60, TimeUnit.SECONDS)
              builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
              builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
              builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })
              builder
          } catch (e: Exception) {
              throw RuntimeException(e)
          }

      init {
          initialize(Ikea.Environment.TEST)
      }*/

}