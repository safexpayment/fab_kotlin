package com.fab.android.ikea.ui

import android.view.View
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    /**
     * Attach views to Java in this method
     *
     * @param view rootview of the fragment
     */
    fun inflateXML(view: View?) {
        // No logic
    }

    /**
     * @return Name of the fragment
     */
    val fragmentName: String
        get() = ""
}