package com.fab.android.ikea.constants

object SessionStore {

    //merchant details
    var merchantKey = ""
    var merchantId = ""

    //txn details
    var AGGREGATOR_ID = ""
    var ORDER_NO = ""
    var COUNTRY_CODE = ""
    var ORDER_AMOUNT = ""
    var CURRENCY = ""
    var TXN_TYPE = ""
    var SUCCESS_URL = ""
    var FAILURE_URL = ""
    var CHANNEL = ""

    //environment
    var baseUrl = ""

    //branding details
    var brandingName = ""
    var menuColor = ""
    var headingColor = ""
    var bgColor = ""
    var footerColor = ""
    var brandingLogo = ""
    var PG_ID = ""
    var PAYMODE_ID = ""
    var SCHEME_ID = ""
    var EMI_MONTHS = "7"

    //All details
    var PG_DETAILS = ""
    var CARD_DETAILS = ""
    var CUSTOMER_DETAILS = ""
    var BILLING_DETAILS = ""
    var SHIPPING_DETAILS = ""
    var ITEM_DETAILS = ""
    var UPI_DETAILS = ""
    var OTHER_DETAILS = ""

    //created
    var isResultPage = false

    //Success keys
    var successOrderId = ""
    var successTransactionId = ""
    var successPaymentId = ""

    //Login details
    var fullName = ""
    var mobileNumber = ""
    var mailId = ""

    //Amount Details
    var orderId = ""
    var orderAmount = ""
    var totalAmount = ""
    var totalAmountWithoutSymbol = ""
    var currencySymbol = ""

    //Session end
    fun clearSession() {
        brandingName = ""
        menuColor = ""
        headingColor = ""
        bgColor = ""
        footerColor = ""
        merchantKey = ""
        merchantId = ""
        brandingLogo = ""
        PG_ID = ""
        PAYMODE_ID = ""
        SCHEME_ID = ""
        EMI_MONTHS = "7"
        successOrderId = ""
        successTransactionId = ""
        successPaymentId = ""
        fullName = ""
        mobileNumber = ""
        mailId = ""
        orderId = ""
        orderAmount = ""
        AGGREGATOR_ID = ""
        ORDER_NO = ""
        COUNTRY_CODE = ""
        ORDER_AMOUNT = ""
        CURRENCY = ""
        TXN_TYPE = ""
        SUCCESS_URL = ""
        FAILURE_URL = ""
        CHANNEL = ""
        PG_DETAILS = ""
        CARD_DETAILS = ""
        CUSTOMER_DETAILS = ""
        BILLING_DETAILS = ""
        SHIPPING_DETAILS = ""
        ITEM_DETAILS = ""
        UPI_DETAILS = ""
        OTHER_DETAILS = ""
        isResultPage = false
    }

    //payment clear
    fun clearPaymentIds() {
        PG_ID = ""
        PAYMODE_ID = ""
        SCHEME_ID = ""
    }
}