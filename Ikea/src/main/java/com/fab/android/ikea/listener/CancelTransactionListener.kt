package com.fab.android.ikea.listener

interface CancelTransactionListener {
    fun cancelClicked(reason: String?)
}