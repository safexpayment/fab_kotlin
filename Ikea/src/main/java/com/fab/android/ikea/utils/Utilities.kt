package com.fab.android.ikea.utils

import java.text.NumberFormat
import java.text.ParsePosition

object Utilities {


    /**
     * Return Currency Type
     */
    fun getCurrencyType(currencySymbol: String?): CurrencyTypes {
        for (currencyTypes in CurrencyTypes.values()) {
            if (currencyTypes.matches(currencySymbol!!)) return currencyTypes
        }
        return CurrencyTypes.INR
    }

    /**
     * Return only strings values or character
     */
    fun getDigitsOnlyString(numString: String): String {
        val sb = StringBuilder()
        for (c in numString.toCharArray()) {
            if (Character.isDigit(c)) sb.append(c)
        }
        return sb.toString()
    }

    /**
     * Check text is numeric or not
     */
    fun isNumeric(str: String): Boolean {
        val formatter = NumberFormat.getInstance()
        val pos = ParsePosition(0)
        formatter.parse(str, pos)
        return str.length == pos.index
    }
}